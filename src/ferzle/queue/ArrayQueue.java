package ferzle.queue;

import java.util.Iterator;

/**
 * A simple Queue implementation based on an array
 * 
 * @author Chuck Cusack
 * @version 1.0, February 2008
 */
public class ArrayQueue<T> implements QueueInterface<T> {
	
	// The data
	T[] theElements;

	public ArrayQueue(int capacity) {
		// The following code is illegal in Java.
		// You cannot create an array of a generic type in Java
		// theElements = new T[capacity];

		// There are two "hacks" that allow you to get an array of
		// generic type. Both give compiler warnings.
		theElements = (T[]) new Object[capacity];
		// theElements =
		// (T[])Array.newInstance(theElements.getClass(),capacity);
	}

	@Override
	public boolean isEmpty() {
		// TODO Implement me!
		return false;
	}

	@Override
	public boolean isFull() {
		// TODO Implement me!
		return false;
	}

	@Override
	public boolean enqueue(T item) {
		// TODO Implement me!
		return false;
	}

	@Override
	public T dequeue() {
		// TODO Implement me!
		return null;
	}

	@Override
	public T peek() {
		// TODO Implement me!
		return null;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Implement me if time permits.
		return null;
	}
}
