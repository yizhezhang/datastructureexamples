package ferzle.linkedList;

import ferzle.util.Node;

public class LinkedList<T> {
	Node<T> head;

	public LinkedList() {
		head = null;
	}

	/**
	 * Add value to the head of the list (the first element).
	 * 
	 * @param value
	 *            the value to add to the head of the list.
	 */
	public void insertAtHead(T value) {
		Node<T> node = new Node<T>(value);
		node.next = head;
		head = node;
	}

	/**
	 * Add value to the tail of the list (the last element).
	 * 
	 * @param value
	 */
	public void addToTail(T value) {
		Node<T> node = new Node<T>(value, null);
		Node<T> x = head;
		if (head != null) {
			while (x.next != null) {
				x = x.next;
			}
			x.next = node;
		} else {
			head = node;
		}
	}

	/**
	 * 
	 * @param item
	 * @return true if and only if item is contained in the linked list.
	 */
	public boolean contains(T item) {
		// TODO implement me.
		return false;
	}

	/**
	 * 
	 * @param itemInList
	 *            A (supposed) item that is already in the list
	 * @param itemToInsert
	 *            A new item to add.
	 * @return true if itemInList is on the list and itemToInsert was added
	 *         after it, and false otherwise.
	 */
	public boolean insertAfter(T itemInList, T itemToInsert) {
		// TODO implement me.
		return false;
	}

	/**
	 * 
	 * @param item
	 *            Item to delete from the list
	 * @return true if item was on the list and was deleted, and false
	 *         otherwise.
	 */
	public boolean delete(T item) {
		// TODO implement me.
		return false;
	}
}
